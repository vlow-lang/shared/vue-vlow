export function getRowsInNodeProto(proto) {
    return Math.max(
        Object.keys(proto.inputs || {}).length + Object.keys(proto.passthroughs || {}).length,
        Object.keys(proto.outputs || {}).length + Object.keys(proto.collects || {}).length
    )
}