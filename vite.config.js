import path from "path";
import vue from '@vitejs/plugin-vue'
import { nodeResolve } from '@rollup/plugin-node-resolve';

module.exports = {
	build: {
		lib: {
			entry: path.resolve(__dirname, 'lib/index.js'),
			name: 'vue-vlow'
		},
		rollupOptions: {
			external: ['vue'],
			output: {
				globals: {
					vue: 'Vue'
				}
			}
		}
	},
	plugins: [
		vue(),
		nodeResolve()
	]
}
